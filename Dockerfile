FROM ubuntu:18.04

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y apt-utils \
  curl \
  wget \
  unzip \
  git \
  mysql-client \
  sudo \
  vim \
  bzip2 \
  htop \
  software-properties-common \
  build-essential \
  libssl-dev \
  php7.2-common \
  php-apcu \
  php7.2-curl \
  php7.2-dev \
  php7.2-mysql \
  php7.2-curl \
  php7.2-soap \
  php7.2-intl \
  php7.2-gd \
  php7.2-mbstring \
  php7.2-zip \
  php7.2-xml \
  php7.2-bcmath \
  php-xdebug \
  php-pear \
  php-memcache \
  libapache2-mod-php7.2 \
  libsqlite3-dev \
  ruby \
  ruby-dev \
  command-not-found

# install the utils extensions we need
## COMPOSER ##
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&& php composer-setup.php \
	&& php -r "unlink('composer-setup.php');" \
	&& chmod +x composer.phar \
	&& mv composer.phar /usr/local/bin/composer
## DRUSH ##
#### REmoved by http://docs.drush.org/en/master/install/
## WP CLI ##
RUN wget  https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
	&& chmod +x wp-cli.phar \
	&& mv wp-cli.phar /usr/local/bin/wp
## NODE with NVM, GRUNT, BOWER ##
# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

ENV NVM_DIR /usr/local/nvm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | PROFILE=/etc/bash.bashrc /bin/bash \
	&& source $NVM_DIR/nvm.sh \
	&& nvm install 8 \
    	&& nvm install 6 \
	&& nvm install 4 \
    	&& nvm alias default 8 \
    	&& nvm use default \
	&& npm install npm -g \
	&& npm install -g grunt-cli bower http-server
## YARN ##
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg \
	&& apt-key add -echo "deb https://dl.yarnpkg.com/debian/ stable main" \
	&& tee /etc/apt/sources.list.d/yarn.list \
	&& apt-get update && apt-get install -y yarn

## SOME HACK on PHP ##
RUN rm -f /etc/php/7.2/apache2/conf.d/10-opcache.ini \
	&& rm -f /etc/php/7.2/apache2/conf.d/20-xdebug.ini \
	&& echo "export PHP_INI_SCAN_DIR=/etc/php/7.2/cli/conf.d:/etc/php/7.2/cli/conf.d/overrides" >> ~/.bashrc \
	&& a2enmod rewrite && a2enmod proxy && a2enmod proxy_http

RUN gem install mailcatcher && mailcatcher

ADD conf/10-sameuser-nopass /etc/sudoers.d/
ADD conf/user.sh / 
RUN /user.sh
COPY conf/httpd-foreground /usr/local/bin/

EXPOSE 80
EXPOSE 1025
EXPOSE 1080

WORKDIR /var/www/html

CMD ["httpd-foreground"]
