Lamp devbox
========================

Using docker and Docker compose, install a cantainer based environment to develop using:

 - Ubuntu 18.04
 - Apache 2.4.27
 - php 7.2.10
 - mysql 5.7.14
 - memcache
 - composer
 - drush
 - wp cli
 - nodejs 8, 6 and 4 witn nvm
 - npm
 - grunt
 - bower
 - yarn
 - mailcatcher

1) Install
----------------------------------

Install prerequisites:

 - docker https://docs.docker.com/engine/installation/
 - docker-compose https://docs.docker.com/compose/install/
 
Clone repository in a directory then cd into it
Create a symlink to your project directory and call it *code*

 if you are on Windows or OS X machine, make *code* directory and copy your codebase inside it (this repo must be in user HOM dir)

Start containers using >$ docker-compose up

2) Usage
----------------------------------

All services will be available on ip 0.0.0.0 or 127.0.0.1 (only Linux and Mac OS)

 - Apache container expose port 80
 - xdebug listening on 9009
 - Mailcatcher webserver listen on 1080
 - Mailcatcher smtp listen on 1025
 - mysql container is on 3306
 - memcache container is on 11211
 
you can change ports changing *docker-compose.yml*

To execute composer or othe adds call >$ docker run web /bin/bash

To enter in bash mode on apache or mysql >$ docker exec -it <container name> /bin/bash

There are some little improvements on php, you can change them acting on *Dockerfile* before the first **UP**

 - date.timezone = "Europe/Rome"
 - memory_limit = 512M
 - upload_max_filesize = 80M
 
 On Mysql you can act runtime changing *conf/mysql/add.cnf* file
 You can add virtualhosts writing conf files in *conf/apache/vhosts*

3) VIRTUALHOSTS
----------------------------------

In conf/apache/vhosts you can set virtualhost for apache. To activate chenges, stop containers and restart

4) USERS
----------------------------------

User *sameuser* in *samegroup* will be created with GID and UID you define in ./conf/user.sh
This user shoud sudo without password

5) TIPS
----------------------------------

container names are:

 - **web** for the webserver
 - **db** for the mysql container
 - **memcache** for the memcache container

you need to use this inside configuration (web knows db because the are linked)
